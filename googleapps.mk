# Copyright (C) 2019 Benzo Rom
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# app
PRODUCT_PACKAGES += \
    arcore \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    ConnectivityMonitor \
    Drive \
    FaceLock \
    GoogleCamera \
    GoogleContacts \
    GoogleContactsSyncAdapter \
    GoogleExtShared \
    GoogleTTS \
    GoogleVrCore \
    MarkupGoogle \
    NexusWallpapersStubPrebuilt2018 \
    Photos \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle \
    PrebuiltGmail \
    SoundPickerPrebuilt \
    talkback \
    WallpapersBReel2018

# priv-app
PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    AndroidPlatformServices \
    ConfigUpdater \
    ConnMetrics \
    GCS \
    GoogleBackupTransport \
    GoogleDialer \
    GoogleExtServices \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleServicesFramework \
    MatchmakerPrebuilt \
    NexusLauncherPrebuilt \
    Phonesky \
    PrebuiltGmsCorePi \
    SettingsIntelligenceGooglePrebuilt \
    SetupWizard \
    StorageManagerGoogle \
    TagGoogle \
    TurboPrebuilt \
    Velvet \
    WallpaperPickerGooglePrebuilt \
    WellbeingPrebuilt

# framework
PRODUCT_PACKAGES += \
    com.google.android.dialer.support \
    com.google.android.maps \
    com.google.android.media.effects \
    com.google.widevine.software.drm

# Overlays
PRODUCT_PACKAGE_OVERLAYS += \
    vendor/googleapps/overlay/

# Props
PRODUCT_PROPERTY_OVERRIDES += \
    ro.com.google.ime.bs_theme=true \
    ro.opa.eligible_device=true \
    setupwizard.feature.baseline_setupwizard_enabled=true \
    ro.com.google.ime.theme_id=5 \
    ro.error.receiver.system.apps=com.google.android.gms \
    ro.setupwizard.enterprise_mode=1 \
    ro.atrace.core.services=com.google.android.gms,com.google.android.gms.ui,com.google.android.gms.persistent \
    ro.setupwizard.rotation_locked=true \
    setupwizard.enable_assist_gesture_training=true \
    ro.setupwizard.esim_cid_ignore=00000001 \
    setupwizard.theme=glif_v3_light \
    ro.storage_manager.show_opt_in=true \
    ro.wallpapers_loc_request_suw=true

# Libraries
PRODUCT_COPY_FILES += \
    vendor/googleapps/lib64/libbarhopper.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libbarhopper.so \
    vendor/googleapps/lib64/libgdx.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libgdx.so \
    vendor/googleapps/lib64/libwallpapers-breel-2018-jni.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libwallpapers-breel-2018-jni.so \
    vendor/googleapps/lib64/liblpmdeviceutils.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/liblpmdeviceutils.so \
    vendor/googleapps/lib64/libsketchology_native.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libsketchology_native.so

# Symlinks
PRODUCT_PACKAGES += \
    libbarhopper.so \
    libfacenet.so \
    libgdx.so \
    libwallpapers-breel-2018-jni.so \
    liblpmdeviceutils.so \
    libsketchology_native.so

ifneq ($(filter marlin sailfish, $(TARGET_DEVICE)),)
# Camera and Pixel features
PRODUCT_PACKAGES += \
    com.google.android.camera.experimental2016
PRODUCT_COPY_FILES += \
    vendor/googleapps/etc/permissions/com.google.android.camera.experimental2016.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.camera.experimental2016.xml \
    vendor/googleapps/etc/sysconfig/pixel_2016_exclusive.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_2016_exclusive.xml \
    vendor/googleapps/etc/sysconfig/pixel_experience_2017.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_experience_2017.xml \
    vendor/googleapps/etc/sysconfig/pixel_experience_2018.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_experience_2018.xml
endif
ifneq ($(filter taimen walleye, $(TARGET_DEVICE)),)
# Camera and Pixel 2 features
PRODUCT_COPY_FILES += \
    vendor/googleapps/etc/sysconfig/pixel_2017_exclusive.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_2017_exclusive.xml \
    vendor/googleapps/etc/sysconfig/pixel_experience_2017.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_experience_2017.xml \
    vendor/googleapps/etc/sysconfig/pixel_experience_2018.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_experience_2018.xml
endif
ifneq ($(filter crosshatch bluecross, $(TARGET_DEVICE)),)
# Camera and Pixel 3 features
PRODUCT_PACKAGES += \
    com.google.android.camera.experimental2018 \
    libborders_scone_leveldb_jni.so \
    libhwinfo
PRODUCT_COPY_FILES += \
    vendor/googleapps/etc/permissions/com.google.android.camera.experimental2018.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.camera.experimental2018.xml \
    vendor/googleapps/etc/permissions/com.google.android.hardwareinfo.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.hardwareinfo.xml \
    vendor/googleapps/etc/sysconfig/pixel_2018_exclusive.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_2018_exclusive.xml \
    vendor/googleapps/etc/sysconfig/pixel_experience_2018.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_experience_2018.xml \
    vendor/googleapps/lib64/libborders_scone_leveldb_jni.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libborders_scone_leveldb_jni.so

# Extra Packages
PRODUCT_PACKAGES += \
    Chrome \
    ConferenceDialer \
    HardwareInfo \
    SCONE \
    ScribePrebuilt \
    SoundAmplifierPrebuilt

# Webview Overlay
PRODUCT_PACKAGE_OVERLAYS += \
    vendor/googleapps/overlay-webview/
endif

# Blobs
PRODUCT_COPY_FILES += \
    vendor/googleapps/etc/default-permissions/default-permissions.xml:$(TARGET_COPY_OUT_VENDOR)/etc/default-permissions/default-permissions.xml \
    vendor/googleapps/etc/permissions/com.google.android.dialer.support.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.dialer.support.xml \
    vendor/googleapps/etc/permissions/com.google.android.maps.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.maps.xml \
    vendor/googleapps/etc/permissions/com.google.android.media.effects.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.media.effects.xml \
    vendor/googleapps/etc/permissions/com.google.widevine.software.drm.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.widevine.software.drm.xml \
    vendor/googleapps/etc/permissions/privapp-permissions-googleapps.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-googleapps.xml \
    vendor/googleapps/etc/preferred-apps/google.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/preferred-apps/google.xml \
    vendor/googleapps/etc/sysconfig/google.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google.xml \
    vendor/googleapps/etc/sysconfig/google_build.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google_build.xml \
    vendor/googleapps/etc/sysconfig/google-hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google-hiddenapi-package-whitelist.xml \
    vendor/googleapps/etc/sysconfig/google_vr_build.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google_vr_build.xml \
    vendor/googleapps/lib/libfilterpack_facedetect.so:$(TARGET_COPY_OUT_SYSTEM)/lib/libfilterpack_facedetect.so \
    vendor/googleapps/lib/libfrsdk.so:$(TARGET_COPY_OUT_SYSTEM)/lib/libfrsdk.so \
    vendor/googleapps/lib64/libfacenet.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libfacenet.so \
    vendor/googleapps/lib64/libfrsdk.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libfrsdk.so \
    vendor/googleapps/lib64/libfilterpack_facedetect.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libfilterpack_facedetect.so \
    vendor/googleapps/usr/srec/en-US/c_fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/c_fst \
    vendor/googleapps/usr/srec/en-US/clg:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/clg \
    vendor/googleapps/usr/srec/en-US/commands.abnf:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/commands.abnf \
    vendor/googleapps/usr/srec/en-US/compile_grammar.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/compile_grammar.config \
    vendor/googleapps/usr/srec/en-US/contacts.abnf:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/contacts.abnf \
    vendor/googleapps/usr/srec/en-US/dict:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/dict \
    vendor/googleapps/usr/srec/en-US/dictation.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/dictation.config \
    vendor/googleapps/usr/srec/en-US/dnn:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/dnn \
    vendor/googleapps/usr/srec/en-US/endpointer_dictation.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/endpointer_dictation.config \
    vendor/googleapps/usr/srec/en-US/endpointer_voicesearch.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/endpointer_voicesearch.config \
    vendor/googleapps/usr/srec/en-US/ep_acoustic_model:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/ep_acoustic_model \
    vendor/googleapps/usr/srec/en-US/g2p_fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/g2p_fst \
    vendor/googleapps/usr/srec/en-US/grammar.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/grammar.config \
    vendor/googleapps/usr/srec/en-US/hclg_shotword:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/hclg_shotword \
    vendor/googleapps/usr/srec/en-US/hmmlist:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/hmmlist \
    vendor/googleapps/usr/srec/en-US/hmm_symbols:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/hmm_symbols \
    vendor/googleapps/usr/srec/en-US/hotword.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/hotword.config \
    vendor/googleapps/usr/srec/en-US/hotword_classifier:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/hotword_classifier \
    vendor/googleapps/usr/srec/en-US/hotword_normalizer:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/hotword_normalizer \
    vendor/googleapps/usr/srec/en-US/hotword_prompt.txt:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/hotword_prompt.txt \
    vendor/googleapps/usr/srec/en-US/hotword_word_symbols:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/hotword_word_symbols \
    vendor/googleapps/usr/srec/en-US/metadata:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/metadata \
    vendor/googleapps/usr/srec/en-US/normalizer:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/normalizer \
    vendor/googleapps/usr/srec/en-US/norm_fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/norm_fst \
    vendor/googleapps/usr/srec/en-US/offensive_word_normalizer:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/offensive_word_normalizer \
    vendor/googleapps/usr/srec/en-US/phonelist:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/phonelist \
    vendor/googleapps/usr/srec/en-US/phone_state_map:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/phone_state_map \
    vendor/googleapps/usr/srec/en-US/rescoring_lm:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/rescoring_lm \
    vendor/googleapps/usr/srec/en-US/wordlist:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/wordlist

# Pixel 2/3 Only
ifneq ($(filter crosshatch bluecross taimen walleye, $(TARGET_DEVICE)),)
PRODUCT_COPY_FILES += \
    vendor/googleapps/usr/share/ime/google/d3_lms/ko_2018030706.zip:$(TARGET_COPY_OUT_SYSTEM)/usr/share/ime/google/d3_lms/ko_2018030706.zip \
    vendor/googleapps/usr/share/ime/google/d3_lms/mozc.data:$(TARGET_COPY_OUT_SYSTEM)/usr/share/ime/google/d3_lms/mozc.data \
    vendor/googleapps/usr/share/ime/google/d3_lms/zh_CN_2018030706.zip:$(TARGET_COPY_OUT_SYSTEM)/usr/share/ime/google/d3_lms/zh_CN_2018030706.zip \
    vendor/googleapps/usr/srec/en-US/am_phonemes.syms:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/am_phonemes.syms \
    vendor/googleapps/usr/srec/en-US/app_bias.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/app_bias.fst \
    vendor/googleapps/usr/srec/en-US/APP_NAME.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/APP_NAME.fst \
    vendor/googleapps/usr/srec/en-US/APP_NAME.syms:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/APP_NAME.syms \
    vendor/googleapps/usr/srec/en-US/CLG.prewalk.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/CLG.prewalk.fst \
    vendor/googleapps/usr/srec/en-US/config.pumpkin:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/config.pumpkin \
    vendor/googleapps/usr/srec/en-US/confirmation_bias.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/confirmation_bias.fst \
    vendor/googleapps/usr/srec/en-US/CONTACT_NAME.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/CONTACT_NAME.fst \
    vendor/googleapps/usr/srec/en-US/CONTACT_NAME.syms:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/CONTACT_NAME.syms \
    vendor/googleapps/usr/srec/en-US/contacts_bias.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/contacts_bias.fst \
    vendor/googleapps/usr/srec/en-US/contacts_disambig.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/contacts_disambig.fst \
    vendor/googleapps/usr/srec/en-US/embedded_class_denorm.mfar:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/embedded_class_denorm.mfar \
    vendor/googleapps/usr/srec/en-US/embedded_normalizer.mfar:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/embedded_normalizer.mfar \
    vendor/googleapps/usr/srec/en-US/endpointer_model:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/endpointer_model \
    vendor/googleapps/usr/srec/en-US/endpointer_model.mmap:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/endpointer_model.mmap \
    vendor/googleapps/usr/srec/en-US/ep_portable_mean_stddev:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/ep_portable_mean_stddev \
    vendor/googleapps/usr/srec/en-US/ep_portable_model.uint8.mmap:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/ep_portable_model.uint8.mmap \
    vendor/googleapps/usr/srec/en-US/g2p.data:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/g2p.data \
    vendor/googleapps/usr/srec/en-US/g2p_graphemes.syms:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/g2p_graphemes.syms \
    vendor/googleapps/usr/srec/en-US/g2p_phonemes.syms:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/g2p_phonemes.syms \
    vendor/googleapps/usr/srec/en-US/input_mean_std_dev:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/input_mean_std_dev \
    vendor/googleapps/usr/srec/en-US/lexicon.U.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/lexicon.U.fst \
    vendor/googleapps/usr/srec/en-US/lstm_model.uint8.data:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/lstm_model.uint8.data \
    vendor/googleapps/usr/srec/en-US/magic_mic.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/magic_mic.config \
    vendor/googleapps/usr/srec/en-US/media_bias.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/media_bias.fst \
    vendor/googleapps/usr/srec/en-US/monastery_config.pumpkin:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/monastery_config.pumpkin \
    vendor/googleapps/usr/srec/en-US/offensive_word_normalizer.mfar:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/offensive_word_normalizer.mfar \
    vendor/googleapps/usr/srec/en-US/offline_action_data.pb:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/offline_action_data.pb \
    vendor/googleapps/usr/srec/en-US/portable_lstm:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/portable_lstm \
    vendor/googleapps/usr/srec/en-US/portable_meanstddev:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/portable_meanstddev \
    vendor/googleapps/usr/srec/en-US/pumpkin.mmap:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/pumpkin.mmap \
    vendor/googleapps/usr/srec/en-US/read_items_bias.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/read_items_bias.fst \
    vendor/googleapps/usr/srec/en-US/rescoring.fst.compact:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/rescoring.fst.compact \
    vendor/googleapps/usr/srec/en-US/semantics.pumpkin:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/semantics.pumpkin \
    vendor/googleapps/usr/srec/en-US/skip_items_bias.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/skip_items_bias.fst \
    vendor/googleapps/usr/srec/en-US/SONG_NAME.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/SONG_NAME.fst \
    vendor/googleapps/usr/srec/en-US/SONG_NAME.syms:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/SONG_NAME.syms \
    vendor/googleapps/usr/srec/en-US/time_bias.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/time_bias.fst \
    vendor/googleapps/usr/srec/en-US/transform.mfar:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/transform.mfar \
    vendor/googleapps/usr/srec/en-US/voice_actions_compiler.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/voice_actions_compiler.config \
    vendor/googleapps/usr/srec/en-US/voice_actions.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/voice_actions.config \
    vendor/googleapps/usr/srec/en-US/word_confidence_classifier:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/word_confidence_classifier \
    vendor/googleapps/usr/srec/en-US/wordlist.syms:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/wordlist.syms
endif
